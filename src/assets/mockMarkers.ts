import { Markets } from '../app/interfaces/markets';
import { v4 as uuid} from 'uuid';

export const MARKER: Markets[] = [
    {id: uuid(), title: 'Museo Casa de la Memoria', description: `Museo solemne con exhibiciones que ofrecen
    diversas perspectivas de guerras y conflictos armados de Colombia.`,
    pathImage: `https://lh5.googleusercontent.com/p/AF1QipMN7z8RtLq4T-VEyL7EDke0WprFzauZvaea0YGi=w408-h271-k-no`,
    latitude: 6.2457399, longitude: -75.5586679},
    {id: uuid(), title: 'Institución Universitaria Escolme', description: `Universidad privada.`,
    pathImage: `https://lh5.googleusercontent.com/p/AF1QipNLURA9wZd-2HuOnDIO_CsNqtUj0Liw0VSs5Mxa=w408-h271-k-no`,
    latitude: 6.2453933, longitude: -75.5607225}
];
