export interface Response {
    status: boolean;
    data: object;
    message: string;
    error: string;
}
