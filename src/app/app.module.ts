import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MapViewComponent } from './components/map-view/map-view.component';
import { AddmarkersComponent } from './components/addmarkers/addmarkers.component';

// custom modules
import { ModalModule } from './components/modal';
import { ListMarkersComponent } from './components/list-markers/list-markers.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';

@NgModule({
  declarations: [
    AppComponent,
    MapViewComponent,
    AddmarkersComponent,
    ListMarkersComponent,
    NavBarComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ModalModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
