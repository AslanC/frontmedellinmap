import * as firebase from 'firebase/app';
import 'firebase/storage';
import 'firebase/firestore';

import { environment } from '../../environments/environment';

export class Firebase {

    static app: firebase.app.App;
    storage: firebase.storage.Reference;
    firestore: firebase.firestore.Firestore;

    constructor() {
        this.init();
    }

    init() {
        if (Firebase.app === undefined) {
            Firebase.app = firebase.initializeApp(environment.firebase);
        }
        this.storage = firebase.storage().ref();
        this.firestore = firebase.firestore();
    }

    Firestore(): firebase.firestore.Firestore {
        return this.firestore;
    }

    StorageRef(): firebase.storage.Reference {
        return this.storage;
    }

    propertyTaskChange(): string {
        return firebase.storage.TaskEvent.STATE_CHANGED;
    }
}
