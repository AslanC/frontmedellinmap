import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { Markets } from '../interfaces/markets';
import { Firebase } from '../class/firebase';
import { Response } from '../interfaces/response';
import { Collections } from '../enums/collections.enum';
import { async } from '@angular/core/testing';



@Injectable({
  providedIn: 'root'
})
export class CreatemarkerService {

  private lat: string; lon: string;
  db: firebase.firestore.Firestore;

  create = false;
  firstCall = false;

  constructor() {
    const fb = new Firebase();
    this.db = fb.Firestore();
  }

  createMarker() {
    this.create = !this.create;
  }

  addMarker(latitude: string, longitude: string) {
    this.lat = latitude;
    this.lon = longitude;
  }

  getDataAddMarker(): Observable<{ lat, lon }> {
    return of({
      lat: this.lat,
      lon: this.lon
    });
  }

  getMarkers(): Observable<Markets> {
    try {
      return new Observable<Markets>(subscriber => {
        this.db.collection(Collections.markers).onSnapshot(
          async (snapshot) => {
            await snapshot.forEach(doc => {
              subscriber.next(doc.data() as Markets);
            });
          }, err => {
            subscriber.error(err);
          });
      });
    } catch (error) {
      throw error;
    }
  }

  postMarker(marker: Markets): Promise<Response> {
    return new Promise<Response>(async (resolve, reject) => {
      try {
        const docRef = await this.db.collection('markersRegistered').doc(`${Date.now().toString()}`);
        const path = docRef.path.split('/')[1];
        docRef.set(marker);
        docRef.update({ pathFile: path });
        docRef.get()
          .then((doc) => {
            const response: Response = {
              status: true,
              message: 'succes',
              data: doc.data(),
              error: ''
            };
            resolve(response);
          })
          .catch(error => {
            reject(error);
          });
      } catch (error) {
        reject(error);
      }
    });
  }

  stopListenonSnapshot() {
    const unsub = this.db.collection(Collections.markers).onSnapshot(() => { });
    // stop lisent for changes
    unsub();
  }

  deleteDocument(marker: Markets): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.db.collection(Collections.markers).doc(marker.pathFile).delete()
        .then(result => {
          if (result !== undefined) {
            return resolve(true);
          }
          return resolve(false);
        })
        .catch(error => {
          reject(error);
        });
    });
  }

  updateMarker(marker: Markets): Promise<boolean> {

    return new Promise(async (resolve, reject) => {
      await this.db.collection(Collections.markers).doc(marker.pathFile).update(marker).then(_ => {
        resolve(true);
      }).catch(error => reject(error));
    });
  }

}
