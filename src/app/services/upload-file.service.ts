import { Injectable } from '@angular/core';
import { Firebase } from '../class/firebase';

@Injectable({
  providedIn: 'root'
})
export class UploadFileService {

  storage: firebase.storage.Reference;
  stateChanged: string;
  constructor() {
    const fb = new Firebase();
    this.storage = fb.StorageRef();
    this.stateChanged = fb.propertyTaskChange();
  }


  postImageMarker(file: File): Promise<{ state, url, message }> {
    class Response {
      state: boolean;
      url: string;
      message: string;
    }

    return new Promise<Response>((resolve, reject) => {
      const metadata = {
        contentType: 'image/jpeg',
        name: file.name
      };
      // file ref
      const refFile = this.storage.child(file.name);
      // upload data and attach task
      const uploadTask = refFile.put(file, metadata);
      uploadTask.on(this.stateChanged, null,
        (error) => {
          reject(error);
          console.log(error);
        }, () => {
          uploadTask.snapshot.ref.getDownloadURL()
            .then((url) => {
              const response: Response = {
                state: true,
                url: url.toString(),
                message: ''
              };
              resolve(response);
            })
            .catch(err => {
              const response: Response = {
                state: false,
                url: '',
                message: err
              };
              reject(response);
            });
        });

    });
  }

}
