import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MapViewComponent } from './components/map-view/map-view.component';
import { AddmarkersComponent} from './components/addmarkers/addmarkers.component';
import { ListMarkersComponent } from './components/list-markers/list-markers.component';


const routes: Routes = [
  {path: '', component: MapViewComponent},
  {path: 'Markers', component: ListMarkersComponent},
  {path: 'addmarker', component: AddmarkersComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
