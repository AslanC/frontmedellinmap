export class CMarkets {
    id: string;
    title: string;
    description: string;
    pathImage: string;
    latitude: number;
    longitude: number;
}
