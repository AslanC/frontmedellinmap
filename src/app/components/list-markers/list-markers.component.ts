import { Component, OnInit, OnDestroy } from '@angular/core';

import { CreatemarkerService } from '../../services/createmarker.service';
import { Markets } from '../../interfaces/markets';
import { async } from '@angular/core/testing';

interface HtmlInputEvent extends Event {
  target: HTMLInputElement & EventTarget;
}


@Component({
  selector: 'app-list-markers',
  templateUrl: './list-markers.component.html',
  styleUrls: ['./list-markers.component.css']
})
export class ListMarkersComponent implements OnInit, OnDestroy {

  constructor(private createmarkerService: CreatemarkerService) { }

  imgSelected: string | ArrayBuffer;
  img: File;
  markers: Markets[];
  onEdit: boolean[];
  second = true;
  marker: Markets;
  currentMarker: Markets;

  ngOnInit() {
    this.getData();
  }

  ngOnDestroy() {
    this.createmarkerService.stopListenonSnapshot();
  }

  imageSelected(e: HtmlInputEvent): void {
    if (e.target.files && e.target.files[0]) {
      this.img = e.target.files[0];
      // reading img
      const reader = new FileReader();
      reader.onload = r => this.imgSelected = reader.result;
      reader.readAsDataURL(this.img);

      const element: HTMLElement = document.getElementById('image');
      element.style.maxWidth = '147px';
    }
  }

  getData() {
    this.markers = [];
    this.onEdit = [];

    this.createmarkerService.getMarkers().subscribe(async observe => {
      const result = await this.findObserve(observe);
      if (result) {
        this.markers.push(observe);
        this.onEdit.push(false);
      }
    }, (error) => {
      alert('hubo un error al guardar');
      console.log(error);
    });
  }

  action(key: string, index: number) {
    const img = document.getElementById(`image${index}`);
    const btnCancelAndDelete = document.getElementById(`btnCancelAndDelete${index}`) as HTMLInputElement;
    const btnEditAndSave = document.getElementById(`btnEditAndSave${index}`) as HTMLInputElement;

    switch (key) {
      case 'Editar':
        this.onEdit[index] = true;
        btnCancelAndDelete.innerText = 'Cancelar';
        btnCancelAndDelete.value = 'Cancelar';
        btnEditAndSave.innerText = 'Guardar';
        btnEditAndSave.value = 'Guardar';

        img.classList.remove('disableImage');
        break;
      case 'Guardar':
        this.createmarkerService.updateMarker(this.markers[index]).then(_ => {
          btnCancelAndDelete.innerText = 'Eliminar';
          btnCancelAndDelete.value = 'Eliminar';
          btnEditAndSave.innerText = 'Editar';
          btnEditAndSave.value = 'Editar';
          this.onEdit[index] = false;
          alert('se ha actualizado con exito');
        }).catch(error => {
          console.log(error);
          alert('Hubo un error al actualizar');
        });
        break;
      case 'Eliminar':
        this.second = true;
        this.createmarkerService.deleteDocument(this.markers[index]).catch(err => {
          console.log(err);
        }).then((result) => {
          if (result) {
            this.markers.splice(index, 1);
          }
        });
        break;
      case 'Cancelar':
        this.onEdit[index] = false;
        this.marker = {
          description: '',
          latitude: undefined,
          longitude: undefined,
          title: '',
          pathFile: '',
          pathImage: '',
          id: ''
        };
        btnCancelAndDelete.innerText = 'Eliminar';
        btnCancelAndDelete.value = 'Eliminar';
        btnEditAndSave.innerText = 'Editar';
        btnEditAndSave.value = 'Editar';

        img.classList.add('disableImage');

        break;

      default:
        break;
    }
  }

  findObserve(oneMarker: Markets): Promise<boolean> {
    return new Promise(async (resolve, reject) => {
      await this.markers.map(async (marker) => {
        if (marker.id === oneMarker.id) {
          return resolve(false);
        }
      });
      return resolve(true);
    });
  }

}
