import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {

  constructor(private location: Location) { }

  historyNavigation = false;

  ngOnInit() {
    const history: {navigationId: number} = this.location.getState() as {navigationId};
    if (history.navigationId > 1) {
      console.log('entró');
      this.historyNavigation = true;
    }
  }

  back(): void {
    const history: {navigationId: number} = this.location.getState() as {navigationId};
    if (history.navigationId > 1) {
      this.location.back();
    }
  }
}
