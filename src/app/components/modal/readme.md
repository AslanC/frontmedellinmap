1- importar el modulo del modal en los imports del app.module.ts
ModalModule

2- importar el servicio del modal en el file.component.ts del componenete que lo va a usar e inyectarlo
3- hacerlo publico para que la vista pueda acceder a el
4- ejemplo de uso en la vista, siempre debe tener un id

    <!--  Implementar Modal, para hacer que aparezca directamente, sin usar un boton, quitar el display:none de la etiqueta app-modal
        
    <button (click)="modalService.open('modal-1');">abrir modal</button>
    <app-modal id="modal-1" style="display: none;">
        <p>Somethin text</p>
        <button (click)="modalService.close('modal-1');">cerrar modal</button>
    </app-modal> -->    