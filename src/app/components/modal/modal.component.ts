
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ViewEncapsulation, ElementRef, Input } from '@angular/core';

import { ModalService } from './modal.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ModalComponent implements OnInit, OnDestroy {

  @Input() id: string;

  private element: any;

  constructor(
    private modalService: ModalService,
    private elemntRef: ElementRef

  ) {
      this.element = elemntRef.nativeElement;
    }

  ngOnInit() {
    if (!this.id) {
      console.error('el modal debe tener un id');
      return;
    }

    this.configAndCreate();
  }

  // Configuración del modal
  configAndCreate(): void {

    // poner el elemento (modal) al final del documento (antes de la etiqueta >/body>)
    document.body.appendChild(this.element);

    // cerrar el modal con un clic en el fondo negro
    this.element.addEventListener('click', elemen => {
      if (elemen.target.className === 'bgmodal') {
        this.close();
      }
    });

    this.modalService.add(this);
  }

  ngOnDestroy(): void {
    this.modalService.remove(this.id);
    this.element.remove();
  }

  open(): void {
    this.element.style.display = 'block';
    document.body.classList.add('modal-open');
  }

  close(): void {
    this.element.style.display = 'none';
    document.body.classList.remove('modal-open');
  }

}
