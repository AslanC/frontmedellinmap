import {Injectable} from '@angular/core';

@Injectable({providedIn: 'root'})
export class ModalService {
    private activeModals: any[] = [];

    add(modal: any) {
        this.activeModals.length === 1 ? this.activeModals.length[0] = modal :
        this.activeModals.push(modal);
    }

    remove(id: string) {
        this.activeModals = this.activeModals.filter(x => id !== x.id);
    }

    open(id: string) {
        this.activeModals.find(x => x.id === id).open();
    }

    close(id: string) {
        this.activeModals.find(x => x.id === id).close();
    }
}
