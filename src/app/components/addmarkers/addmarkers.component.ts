import { Component, OnInit, ViewChild } from '@angular/core';
import { v4 as uuid } from 'uuid';

import { CreatemarkerService } from '../../services/createmarker.service';
import { UploadFileService } from '../../services/upload-file.service';
import { Markets } from '../../interfaces/markets';
import { ModalService } from '../modal/modal.service';


interface HtmlInputEvent extends Event {
  target: HTMLInputElement & EventTarget;
}


@Component({
  selector: 'app-addmarkers',
  templateUrl: './addmarkers.component.html',
  styleUrls: ['./addmarkers.component.css']
})

export class AddmarkersComponent implements OnInit {

  img: File;
  imgSelected: string | ArrayBuffer;
  title: string;
  description: string;
  latitude: number;
  longitude: number;
  pathImage: string;

  constructor(public createmarkerServices: CreatemarkerService,
              private uploadFileService: UploadFileService,
              public modalService: ModalService) { }

  ngOnInit() {
  }

  @ViewChild('formContent', {static: false}) set formContent(element) {
    if (element) {
      this.loadData();
    }
  }

  addMarkerForm() {
    this.createmarkerServices.createMarker();
    this.cleanForm();
    this.imgSelected = '';
  }

  imageSelected(e: HtmlInputEvent): void {
    if (e.target.files && e.target.files[0]) {
      this.img = e.target.files[0];
      // reading img
      const reader = new FileReader();
      reader.onload = r => this.imgSelected = reader.result;
      reader.readAsDataURL(this.img);

      const element: HTMLElement = document.getElementById('imgform');
      element.style.maxWidth = '147px';
    }
  }

  async createdMarker() {
    try {
      if (this.title === undefined || this.title === '' || this.imgSelected === undefined || this.imgSelected === ''
        || this.latitude === undefined || this.longitude === undefined) {
        window.alert('debe llenar todos los campos requeridos (*)');
        return false;
      }
      document.getElementById('modalParagraph').innerHTML = 'cargando información';
      this.modalService.open('modalAddedMarker');
      await this.uploadFileService.postImageMarker(this.img).then( async (data) => {
        const marker: Markets = {
          id: uuid(),
          title: this.title,
          description: this.description || 'add a little decription',
          pathImage: data.url,
          latitude: this.latitude,
          longitude: this.longitude
        };
        const response = await this.createmarkerServices.postMarker(marker);
        if (response.status) {
          document.getElementById('modalParagraph').innerHTML = `se ha cargado con exito el marker: ${marker.title}`;
          this.cleanForm();
        } else {
          document.getElementById('modalParagraph').innerHTML = 'hubo un error al subir el marker';
          this.modalService.close('modalAddedMarker');
        }
      });
    } catch (error) {
      console.log(error);
    }
  }

  loadData() {
    this.createmarkerServices.getDataAddMarker().subscribe(data => {
      if (data.lat !== undefined && data.lon !== undefined) {
        this.latitude = data.lat;
        this.longitude = data.lon;
      }
    });
  }

  cleanForm() {
    this.title = undefined;
    this.description = undefined;
    this.latitude = undefined;
    this.longitude = undefined;
    this.imgSelected = undefined;
  }

}
