import { Component, AfterViewInit, OnDestroy } from '@angular/core';
import * as L from 'leaflet';

import { CreatemarkerService } from '../../services/createmarker.service';
import { Markets } from '../../interfaces/markets';

@Component({
  selector: 'app-map-view',
  templateUrl: './map-view.component.html',
  styleUrls: ['./map-view.component.css']
})
export class MapViewComponent implements AfterViewInit, OnDestroy {

  mymap: any;
  zoomlevel: 18;
  markers: Markets;
  address = {
    lat: 6.2440123,
    lng: -75.5644477
  };

  constructor(private createmarkerService: CreatemarkerService) {
  }

  ngAfterViewInit() {
    this.initMap();
    this.addMarkers();
    this.mymap.on('click', e => this.onMapclick(e, this));
  }

  ngOnDestroy() {
    this.createmarkerService.stopListenonSnapshot();
  }

  initMap() {
    this.mymap = L.map('mapid').setView([this.address.lat, this.address.lng], 12);

    // tslint:disable-next-line: max-line-length
    const mainLayer = L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
      maxZoom: 20,
      attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
        '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
        'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
      id: 'mapbox/streets-v11',
      tileSize: 512,
      zoomOffset: -1
    });

    mainLayer.addTo(this.mymap);
  }

  addMarkers() {
    try {
      this.createmarkerService.getMarkers().subscribe((data) => {
        this.markers = data;
        const myicon = L.icon({
          iconUrl: 'assets/pinkMarker.png',
          iconSize: [38, 38],
          iconAnchor: [22, 39],
          popupAnchor: [-3, -36],
          shadowUrl: 'my-icon-shadow.png',
          shadowSize: [68, 95],
          shadowAnchor: [22, 94]
        });
        const optionIcon = {icon: myicon};
        const marker = L.marker([data.latitude, data.longitude], optionIcon);
        const htmlContent = `<img src="${data.pathImage}" style="max-width: 147px; display: block; margin: 0 auto;"' +
            'alt="bird image"><br><b>${data.title}</b><br>${data.description}.`;
        marker.bindPopup(htmlContent).openPopup();
        marker.addTo(this.mymap);
      });
    } catch (error) {
      console.log(error);
    }
  }

  addJustPopup() {
    // set popup as layer
    const poPup = L.popup()
      .setLatLng([6.2445465, -75.5648939])
      .setContent('other pupup')
      .openOn(this.mymap);
  }

  openCreateMarkerForm(e: any) {
    if (!this.createmarkerService.create) {
      this.createmarkerService.addMarker(e.latlng.lat, e.latlng.lng);
      this.createmarkerService.createMarker();
    }
  }

  onMapclick(e, scope) {
    console.log(e);
    const previousButton = document.getElementById('btnYes');
    if (previousButton !== null) {
      previousButton.remove();
    }
    const popup = L.popup();
    const stylewraper = 'font-family: Arial, Helvetica, sans-serif; text-align: center; font-weight: bold; margin: 10px 0;';
    const row = 'margin: 0 auto; display: table;';
    const cel = 'margin: 0 auto; display: table;';
    const btn = `margin: 0 6px; padding: 0px; background-color: #2d2d5d; border-radius: 5px; border: 0px solid transparent;
    text-align: center; font-size: 1em; color: white; width: 50px; height: 33px; cursor: pointer;
    outline: -webkit-focus-ring-color auto 0`;

    const content = `<div class="wrappermarker" style="${stylewraper}"> <div class="row" style="${row}"> <div class="cel" style="${cel}">
      <h4>¿Desea agregar esta dirección en el mapa?</h4> </div> <div class="row" style="${row}">
      <button id="btnYes" style="${btn}">SI</button> <button id="btnNo" style="${btn}">NO</button> </div> </div> </div>`;
    popup
      .setLatLng(e.latlng)
      .setContent(content + e.latlng.toString())
      .openOn(this.mymap);

    document.getElementById('btnYes').onclick = function s() {
      scope.mymap.closePopup();
      scope.openCreateMarkerForm(e);
    };

    document.getElementById('btnNo').onclick = function s() {
      scope.mymap.closePopup();
    };
  }
}
