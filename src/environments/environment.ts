// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyBAnELE1s0Lib6X1EJVxpDMwiiHknnRC48',
    authDomain: 'markets-9a5f2.firebaseapp.com',
    databaseURL: 'https://markets-9a5f2.firebaseio.com',
    projectId: 'markets-9a5f2',
    storageBucket: 'markets-9a5f2.appspot.com',
    messagingSenderId: '484835195706',
    appId: '1:484835195706:web:22438b8f0e5b217af7ae03'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
